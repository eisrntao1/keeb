# Eisrntao's keyboard layouts
These are my personal layouts for the [cheapino](https://github.com/tompi/cheapino) keyboard.

## Deploying

[This](https://github.com/tompi/cheapino/blob/master/doc/buildguide_v1.md) is the author's build guide. Or you can just flash the finished *uf2* files, by first holding **boot**, then plugging in the controller, and then sending the file to it.
